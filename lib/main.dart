import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_beat_indicator.dart';
import 'package:loading/loading.dart';
import 'package:provider/provider.dart';

import './providers/tracks.dart';
import './song_player.dart';
import './themes.dart';

void main() async {
  // var credentials = SpotifyApiCredentials("c0b9cb1ecd674b5ea67d1b5946775a6b", "b3e238d4bc344fad951d452c8fbe7b42");
  // var spotify = SpotifyApi(credentials);

  // var search = await spotify.search
  //     .get('waka waka')
  //     .first(2)
  //     .catchError((err) => print((err as SpotifyException).message));
  // if (search == null) {
  //   return;
  // }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => Tracks(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          // theme: ThemeData(
          //   // This is the theme of your application.
          //   //
          //   // Try running your application with "flutter run". You'll see the
          //   // application has a blue toolbar. Then, without quitting the app, try
          //   // changing the primarySwatch below to Colors.green and then invoke
          //   // "hot reload" (press "r" in the console where you ran "flutter run",
          //   // or simply save your changes to "hot reload" in a Flutter IDE).
          //   // Notice that the counter didn't reset back to zero; the application
          //   // is not restarted.
          //   primarySwatch: Colors.teal,
          // ),
          theme: lightTheme,
          home: SpotifyDemo()),
    );
  }
}

class SpotifyDemo extends StatefulWidget {
  @override
  _SpotifyDemoState createState() => _SpotifyDemoState();
}

class _SpotifyDemoState extends State<SpotifyDemo> {
  TextEditingController textController = new TextEditingController();
  bool showSearch = false;
  var currentIndex = -1;

  var _progressIndicator = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: showSearch
            ? AppBar(
                leading: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    setState(() {
                      this.textController.text = "";
                      this.showSearch = false;
                    });
                  },
                ),
                title: TextField(
                  controller: textController,
                  textInputAction: TextInputAction.go,
                  onSubmitted: (data)async{
                    print(data);
                    await Provider.of<Tracks>(context, listen: false)
                          .getSpotifyData(this.textController.text);
                      this.textController.text = "";
                      setState(() {
                        this.showSearch = false;
                      });
                  },
                ),
                actions: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    onPressed: () async {
                      await Provider.of<Tracks>(context, listen: false)
                          .getSpotifyData(this.textController.text);
                      this.textController.text = "";
                      setState(() {
                        this.showSearch = false;
                      });
                    },
                  )
                ],
              )
            : AppBar(
                // title: Text("Spotify Home"),
                // title: Container(
                //   margin: EdgeInsets.fromLTRB(0, 25, 0, 25),
                //   height: 60,
                //   child: Image(
                //     image: new AssetImage("assets/appicon.png"),
                //     // color: Colors.black54,
                //     fit: BoxFit.fill,
                //     // height: 100,
                //     colorBlendMode: BlendMode.darken,
                //   ),
                // ),
                title: Text("Music Player"),
                actions: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      setState(() {
                        this.showSearch = true;
                      });
                    },
                  )
                ],
              ),
        body: Container(
            height: 600,
            // color: Colors.green,
            margin: EdgeInsets.all(10.00),
            child: FutureBuilder(
              future: Provider.of<Tracks>(context).items,
              builder: (ctx, snapshot) {
                if (snapshot.data == null || snapshot.data.length == 0) {
                  //print('project snapshot data is: ${projectSnap.data}');
                  return Container(
                    child: Center(
                      child: Text("Nothing To Show"),
                    ),
                  );
                }

                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        // dense: true,
                        contentPadding: EdgeInsets.all(10),
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                snapshot.data[index].name,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                snapshot.data[index].artistName,
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),
                          leading: snapshot.data[index].images != null
                              ? Image.network(snapshot.data[index].images[0])
                              : Icon(Icons.cloud_circle),
                          trailing: GestureDetector(
                            child: Icon(
                              Icons.play_arrow,
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        new NowPlaying(
                                      nowPlayTap: true,
                                      songId: snapshot.data[index].id,
                                    ),
                                  ));
                            },
                          ));
                    });
              },
            )));
  }
}
