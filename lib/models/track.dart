

class Track{
  String id;
  String name;
  bool collaborative;
  String href;
  String trackLinkHref;
  String owner;
  String snapshotId;
  String previewUrl;
  String type;
  String uri;
  String artistName;
  String albumName;
  List<String> images;

  Track({this.id,this.name,this.collaborative,this.href,this.trackLinkHref,this.owner,this.snapshotId,this.uri,this.type,this.previewUrl,this.albumName,this.artistName}){

  }
}