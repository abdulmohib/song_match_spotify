import 'package:flutter/material.dart';

Widget blurWidget(String imageUrl) {
  var f =null;
  return new Hero(
    tag: 'Test',
    child: new Container(
      child: imageUrl != null
          // ignore: conflicting_dart_import
          ? Image.network(
              imageUrl,
              fit: BoxFit.cover,
              color: Colors.black54,
              colorBlendMode: BlendMode.darken,
            )
          : new Image(
              image: new AssetImage("assets/lady.jpeg"),
              color: Colors.black54,
              fit: BoxFit.cover,
              colorBlendMode: BlendMode.darken,
            ),
    ),
  );
}