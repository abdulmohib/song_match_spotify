import 'package:flutter/material.dart';
import '../models/track.dart' as tr;
import 'package:spotify/spotify.dart';

class Tracks with ChangeNotifier{
  List<tr.Track> _items = [];
  var credentials = SpotifyApiCredentials("c0b9cb1ecd674b5ea67d1b5946775a6b", "b3e238d4bc344fad951d452c8fbe7b42");
  

  Future<List<tr.Track>> get items async{
    return [..._items];
  }



  Future<void> getSpotifyData(String keyword)async{
    this._items = [];
    var spotify = SpotifyApi(credentials);
    var search = await spotify.search
      .get(keyword)
      .first(10)
      .catchError((err) => print((err as SpotifyException).message));
    if (search == null) {
      notifyListeners();
      return;
    }
    search.forEach((pages) {
    pages.items.forEach((dynamic item) {
      // if (item is PlaylistSimple) {
      //   tr.Track data = tr.Track(id: item.id,collaborative: item.collaborative,name: item.name,href: item.href,snapshotId: item.snapshotId,uri: item.uri,type: item.type);
      //   if(item.images.length > 0){
      //     data.images =[];
      //     item.images.forEach((x)=>{
      //       data.images.add(x.url)
      //     });
      //   }
      //   this._items.add(data); 
      // }
      // if (item is Artist) {
      //   tr.Track data = tr.Track(id: item.id,name: item.name,href: item.href,uri: item.uri);
      //   if(item.images.length > 0){
      //     data.images =[];
      //     item.images.forEach((x)=>{
      //       data.images.add(x.url)
      //     });
      //   }
      //   this._items.add(data);
      // }
      if (item.type == "track") {
        tr.Track data = tr.Track(id: item.id,name: item.name,href: item.href,previewUrl: item.previewUrl,uri: item.uri,albumName: item.album.name,artistName: item.artists[0].name);
        data.images = [];
        if(item.album.images.length > 0){
          data.images =[];
          item.album.images.forEach((x)=>{
            data.images.add(x.url)
          });
        }
        if(data.previewUrl!=null){
          this._items.add(data);
        }
        
      }
      // if (item is AlbumSimple) {
      //   tr.Track data = tr.Track(id: item.id,name: item.name,href: item.href,uri: item.uri);
      //   if(item.images.length > 0){
      //     data.images =[];
      //     item.images.forEach((x)=>{
      //       data.images.add(x.url)
      //     });
      //   }
      //   this._items.add(data);
      // }
    });
  });
  notifyListeners();
  }


  tr.Track getTrackById(String id){
    var track = this._items.firstWhere((data)=>data.id == id);
    return track;
  }

}