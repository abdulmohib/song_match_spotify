import 'dart:async';
import 'dart:ui';
import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/tracks.dart';

import 'models/track.dart';
import 'widgets/album_ui.dart';
import 'widgets/blur_filter.dart';
import 'widgets/blur_widget.dart';
import 'widgets/control_button.dart';

enum PlayerState { stopped, playing, paused }

class NowPlaying extends StatefulWidget {
  // final Song _song;
  // final SongData songData;
  final String songId;
  bool nowPlayTap;
  NowPlaying({this.nowPlayTap, this.songId});

  @override
  _NowPlayingState createState() => new _NowPlayingState();
}

class _NowPlayingState extends State<NowPlaying> {
  AudioPlayer audioPlayer;
  Duration duration = Duration();
  Duration position = Duration();
  PlayerState playerState;
  // Song song;
  Track track;

  get isPlaying => playerState == PlayerState.playing;
  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';
  get positionText =>
      position != null ? position.toString().split('.').first : '';

  bool isMuted = false;

  @override
  initState() {
    super.initState();
    track =
        Provider.of<Tracks>(context, listen: false).getTrackById(widget.songId);
    initPlayer();
  }

  @override
  void dispose() {
    super.dispose();
    this.audioPlayer.stop();
  }

  // void onComplete() {
  //   setState(() => playerState = PlayerState.stopped);
  //   play(widget.songData.nextSong);
  // }

  initPlayer() async {
    if (audioPlayer == null) {
      audioPlayer = new AudioPlayer();
    }
    setState(() {
      if (widget.nowPlayTap == null || widget.nowPlayTap == false) {
        if (playerState != PlayerState.stopped) {
          stop();
        }
      }
      play(track.previewUrl);
      //  else {
      //   widget._song;
      //   playerState = PlayerState.playing;
      // }
    });

    audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));

    audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = new Duration(seconds: 0);
        position = new Duration(seconds: 0);
      });
    });
  }

  Future play(String url) async {
    if (url != null) {
      await audioPlayer.play(url);

      setState(() {
        playerState = PlayerState.playing;
      });
    }
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() => playerState = PlayerState.paused);
  }

  Future stop() async {
    await audioPlayer.stop();

    setState(() {
      playerState = PlayerState.stopped;
      position = new Duration();
    });
  }

  // Future next(SongData s) async {
  //   stop();
  //   setState(() {
  //     play(s.nextSong);
  //   });
  // }

  // Future prev(SongData s) async {
  //   stop();
  //   play(s.prevSong);
  // }

  Future mute(bool muted) async {
    this.audioPlayer.mute(muted);

    setState(() {
      isMuted = muted;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildPlayer() => new Container(
        padding: new EdgeInsets.all(20.0),
        child: new Column(mainAxisSize: MainAxisSize.min, children: [
          new Column(
            children: <Widget>[
              Text(
                track.name,
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              Text(
                track.artistName,
                style: TextStyle(fontSize: 14, color: Colors.white),
              ),
              Text(
                track.albumName,
                style: TextStyle(fontSize: 14, color: Colors.white),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
              )
            ],
          ),
          new Row(mainAxisSize: MainAxisSize.min, children: [
            new IconButton(
              icon: Icon(
                Icons.skip_previous,
                color: Colors.white60,
              ),
              iconSize: 50,
              onPressed: () {},
            ),
            widget.nowPlayTap
                ? new IconButton(
                    icon: Icon(
                      Icons.pause,
                      color: Colors.white,
                    ),
                    iconSize: 50,
                    onPressed: () {
                      setState(() {
                        widget.nowPlayTap = false;
                        this.audioPlayer.pause();
                      });
                    },
                  )
                : new IconButton(
                    icon: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                    ),
                    iconSize: 50,
                    onPressed: () {
                      setState(() {
                        widget.nowPlayTap = true;
                        this.audioPlayer.play(track.previewUrl);
                      });
                    },
                  ),
            new IconButton(
              icon: Icon(Icons.skip_next, color: Colors.white60),
              iconSize: 50,
              onPressed: () {},
            ),
          ]),
          duration == null
              ? new Container()
              : new Slider(
                  value: position?.inMilliseconds?.toDouble() ?? 0,
                  onChanged: (double value) =>
                      {audioPlayer.seek((value / 1000).roundToDouble())},
                  // audioPlayer.seek((value / 1000).roundToDouble()),
                  min: 0.0,
                  max: duration.inMilliseconds.toDouble()),
          new Row(mainAxisSize: MainAxisSize.min, children: [
            new Text(
                position != null
                    ? "${positionText ?? ''} / ${durationText ?? ''}"
                    : duration != null ? durationText : '',
                // ignore: conflicting_dart_import
                style: new TextStyle(fontSize: 24.0, color: Colors.white))
          ]),
          new Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new IconButton(
                  icon: isMuted
                      ? IconButton(
                          icon: Icon(
                            Icons.headset,
                            color: Theme.of(context).unselectedWidgetColor,
                          ),
                          onPressed: () {
                            this.mute(false);
                          },
                        )
                      : IconButton(
                          icon: new Icon(Icons.headset_off,
                              color: Theme.of(context).unselectedWidgetColor),
                          onPressed: () {
                            this.mute(true);
                          },
                        ),
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    // mute(!isMuted);
                  }),
              // new IconButton(
              //     onPressed: () => mute(true),
              //     icon: new Icon(Icons.headset_off),
              //     color: Colors.cyan),
              // new IconButton(
              //     onPressed: () => mute(false),
              //     icon: new Icon(Icons.headset),
              //     color: Colors.cyan),
            ],
          ),
        ]));

    var playerUI = new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          new AlbumUI(duration, position, track.images[0]),
          new Material(
            child: _buildPlayer(),
            color: Colors.transparent,
          ),
        ]);

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Now Playing"),
        centerTitle: true,
      ),
      body: new Container(
        color: Theme.of(context).backgroundColor,
        child: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            blurWidget(track.images[0]),
            blurFilter(),
            playerUI
          ],
        ),
      ),
    );
  }
}
